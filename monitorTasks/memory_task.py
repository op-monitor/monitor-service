import psutil

from monitorTasks.base_task import Task


class MemoryTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="MemoryMonitor", queue=queue, logger=logger, is_periodic=True)

    def test(self):
        # Calculate Memory for each mounted disk
        # The RAM task is separated and can be found at `RAMTask`
        results = []
        disks = psutil.disk_partitions()
        for disk in disks:
            disk_address = disk.device
            is_opts_fixed = disk.opts == 'rw,fixed'
            disk_usage = psutil.disk_usage(disk_address) if is_opts_fixed else None
            results.append({
                "address": disk_address,
                "usage": {
                    "free": disk_usage.free,
                    "used": disk_usage.used,
                    "total": disk_usage.total,
                    "percent": disk_usage.percent
                } if is_opts_fixed else None
            })

        self.results.update({"disks": results})

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
