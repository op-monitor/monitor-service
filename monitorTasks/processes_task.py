import psutil

from monitorTasks.base_task import Task


class ProcessesTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="ProcessesMonitor", queue=queue, logger=logger, is_periodic=True)

    def test(self):
        # Get active processes (process id, process name, and it's activating user)
        results = []

        for proc in psutil.process_iter():
            try:
                pinfo = proc.as_dict(attrs=['pid', 'name', 'username'])
            except psutil.NoSuchProcess:
                pass
            else:
                results.append(pinfo)

        self.results.update({"processes": results})

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
