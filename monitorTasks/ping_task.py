import subprocess

from monitorTasks.base_task import Task


class PingTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="PingMonitor", queue=queue, logger=logger, is_periodic=True)

    def ping(self, host, timeout=3):
        """
        Send a ping (ICMP) request to a remote host.

        The command is set to ``ping -c 1 -W 1 <host>``, which sends exactly 1 ping
        packet, and waits for 1 second before terminating.

        Args:
            host (str): Hostname or IP address.
            timeout (int): Ping command timeout (in seconds).

        Returns:
            bool: The ping response. ``True`` if the host responds to a ping request
                within the specified timeout period. ``False`` otherwise.

        Note:
            A host may not respond to a ping (ICMP) request even if the host name is
            valid because of firewall rules.
        """

        # Building the command. Ex: "ping -n 1 google.com"
        command = ['ping', host, '-n', '1']

        try:
            subprocess.run(command, timeout=timeout, check=True)
            return True
        except (subprocess.CalledProcessError, subprocess.TimeoutExpired):
            self.logger.warning("Failed to ping host: %s with timeout: %d", host, timeout)
            return False

    def test(self):
        # Get active processes (process id, process name, and it's activating user)
        domains = ['google', 'facebook', "noneexistingdomain123"]
        results = {}
        for host in domains:
            status = self.ping(host + ".com")
            results[host] = status

        self.results.update(results)

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
