import psutil

from monitorTasks.base_task import Task


class CPUTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="CPUMonitor", queue=queue, logger=logger, is_periodic=True)

    def test(self):
        # Calculate CPU
        self.results.update({"cpu": psutil.cpu_percent(interval=1)})
        self.results.update({"cores": psutil.cpu_count(logical=True)})
        self.results.update({"physicalCores": psutil.cpu_count(logical=False)})

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
