import psutil

from monitorTasks.base_task import Task


class RAMTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="RAMMonitor", queue=queue, logger=logger, is_periodic=True)

    def test(self):
        # Calculate Real Access Memory (RAM)
        # The disks memory task is separated and can be found at `MemoryTask`

        # gives an object with many fields - dictionary format
        self.results.update(dict(psutil.virtual_memory()._asdict()))

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
