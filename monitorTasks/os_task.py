import platform

from monitorTasks.base_task import Task


class OSTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="OSMonitor", queue=queue, logger=logger, is_periodic=False)

    def test(self):
        # Get the machine details
        self.results.update({
            'system': platform.system(),
            'node': platform.node(),
            'release': platform.release(),
            'version': platform.version(),
            'machine': platform.machine(),
            'processor': platform.processor(),
            'architecture': platform.architecture(),
        })

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
