from threading import Thread

from utils.threads import every


class Task(object):
    def __init__(self, logger, name, queue, timing=10, is_periodic=False):
        self.logger = logger
        self.timing = timing
        self.name = name
        self.is_active = False
        self.resultsQueue = queue
        self.results = {}
        self.isPeriodic = is_periodic

    def test(self):
        raise NotImplemented

    def parse(self):
        self.resultsQueue.put((self.name, self.results))
        return self.results

    def _start_thread(self, th):
        if not self.is_active:
            th.start()
            self.is_active = True
        else:
            th.run()
        # th.join()

    def _run_build(self):
        """
        Define the execution of each task
        :return:
        """
        self.test()
        self.parse()

    def run(self):
        th = Thread(group=None,
                    target=self._run_build,
                    name=self.name)
        self._start_thread(th)

    def periodic_run(self):
        th = Thread(group=None,
                    target=lambda: every(self.timing, self._run_build),
                    name=self.name)
        self._start_thread(th)
