import psutil

from monitorTasks.base_task import Task


class PortsTask(Task):

    def __init__(self, logger, queue):
        super().__init__(name="PortsMonitor", queue=queue, logger=logger, is_periodic=True)

    def test(self):
        # Get active connections (port, protocols etc)
        results = []
        raw_cons = psutil.net_connections()
        for raw_con in raw_cons:
            results.append({
                "raddr": {
                    "ip": raw_con.raddr.ip,
                    "port": raw_con.raddr.port,
                } if len(raw_con.raddr) > 0 else {},
                "laddr": {
                    "ip": raw_con.laddr.ip,
                    "port": raw_con.laddr.port,
                } if len(raw_con.laddr) > 0 else {},
                "pid": raw_con.pid,
                "status": raw_con.status
            })

        # Using the PC
        self.results.update({"connections": results})

        self.logger.info("%s test completed" % self.name)
        self.logger.info(self.results)
