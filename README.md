# The project architecture:
![op-monitor_architecture](ReadmeAssets/op-monitor_architecture.png)

# op-service
The services installed manually on computers, that use to monitoring its activity over time.

The services separate into `Agent` and `Monitor`.

### Agent
Contains 3 components that run in parallel (different `Threads`)
* `listener` - listen to requests from the API and other `Monitors` and resolve their requests.
* `monitor` - run periodic tasks on the current machine and saves the results on `resultsQueue`.
* `notifier` - iterate over the `resultsQueue` and report to it's registered `Monitors`.

### Monitor - 
* `listener` - listen to requests from the API and other `Agents` to resolve their requests.

## Class Design
![op-service_class_design](ReadmeAssets/op-service_class_design.png)


## Stack:
* Python3.6
* Service installation using [NSSM](https://nssm.cc/):
