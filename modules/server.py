class Server(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def toString(self):
        return "{host}:{port}".format(host=self.host, port=self.port)