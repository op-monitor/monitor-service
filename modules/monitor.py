import socket
from json import JSONDecodeError
from threading import Thread
from jsonschema import validate, ValidationError
import requests

from utils import sockets, strings, logger
from utils.exceptions import UnsupportedRequest
from utils import constants
from modules.server import Server


class MonitorServer(Server):
    def __init__(self, host="127.0.0.1", port=10000):
        super().__init__(host, port)
        self.registeredAgents = []
        self.logger = logger.create("monitor")

    def _listen_main(self):
        """
        Set a listener on dedicated port
        Receive and handle reports updates for the agents, and requests from `os-web-api`
        :return:
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_address = (self.host, self.port)
        self.logger.info('started monitor-listener %s port %s' % server_address)
        sock.bind(server_address)

        while True:
            try:
                sock.settimeout(None)
                data, address = sock.recvfrom(4096 * 8)
                self.logger.info('received %s bytes from %s' % (len(data), address))
                resp = {}
            except TimeoutError:
                continue

            try:
                data = strings.bytes_to_json(data)
                validate(instance=data, schema=constants.monitorReceivePacketSchema)
                self.logger.info(data)
                action, agent = data['action'], data['agent'] if 'agent' in data else None

                if action == "register":
                    status = self.register_agent(agent['host'], agent['port'])
                elif action == "unregister":
                    status = self.unregister_agent(agent['host'], agent['port'])
                elif action == "report":
                    status = self.send_report(data)
                elif action == "ping":
                    status = True
                else:
                    raise UnsupportedRequest("Got an unsupported action from Monitor: %s" % action)

                if status:
                    resp = {"status": "ok"}
                else:
                    resp = {"status": "error"}

            except (UnsupportedRequest, ValidationError, TimeoutError) as e:
                self.logger.warn(str(e))
                resp["message"] = str(e)
            except KeyboardInterrupt:
                self.logger.error("KeyboardInterrupt received, closing connection")
                raise
            finally:
                (host, port) = address
                sockets.send_packet(self.logger, resp, Server(host, port))

    def listen(self):
        """
        Runs `listener` as a thread
        :return:
        """
        Thread(group=None,
               target=self._listen_main(),
               name="Listener").start()

    def register_agent(self, host, port):
        """
        Register an Agent the this Monitor
        :param (string) host: the requested Agent's host
        :param (int) port: the requested  Agent's port
        :return (boolean): registration status
        """
        try:
            agent = Server(host, port)
            status = sockets.send_packet(self.logger, constants.REGISTER_PAYLOAD(self.host, self.port), agent, True)
            if status:
                self.registeredAgents.append(agent)
            return status
        except (ConnectionError, ConnectionRefusedError, TimeoutError) as e:
            self.logger.warn("failed to register agent \r\n%s" % e.message)
            return False

    def unregister_agent(self, host, port):
        """
        Unregister an Agent the this Monitor
        :param (string) host: the requested Agent's host
        :param (int) port: the requested  Agent's port
        :return (boolean): unregistration status
        """
        try:
            status = sockets.send_packet(self.logger, constants.UNREGISTER_PAYLOAD(self.host, self.port), Server(host, port), True)
            if status:  # the agent successfully unregistered
                for registeredAgent in self.registeredAgents:
                    if registeredAgent.host == host and registeredAgent.port == port:
                        self.registeredAgents.remove(registeredAgent)
                        return True
            return False

        except (ConnectionError, ConnectionRefusedError) as e:
            self.logger.warn("failed to register agent \r\n%s" % e.message)
            return False

    def send_report(self, data):
        """
        Send a new report to the `op-web-api`
        :param (dict) data: the report's data
        :return (boolean): the sending-report status
        """
        try:
            #  Add the Monitor data to the request payload
            data["monitor"] = {"host": self.host, "port": self.port}
            res = requests.post(constants.SEND_REPORT_URL, json=data)
            self.logger.info("report res %s" % res.json())
            return True
        except (ConnectionError, ConnectionRefusedError, JSONDecodeError) as e:
            self.logger.warn("failed to send report \r\n%s" % str(e))
            return False
