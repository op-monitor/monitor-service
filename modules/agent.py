import socket
from queue import Queue
from threading import Thread
from json import JSONDecodeError
from jsonschema import validate, ValidationError

from modules.server import Server
from utils import strings, sockets, logger, constants
from utils.exceptions import UnsupportedRequest

from monitorTasks.cpu_task import CPUTask
from monitorTasks.memory_task import MemoryTask
from monitorTasks.ports_task import PortsTask
from monitorTasks.ram_task import RAMTask
from monitorTasks.os_task import OSTask
from monitorTasks.processes_task import ProcessesTask
from monitorTasks.ping_task import PingTask

SOCKET_TIMEOUT = 2  # seconds


class AgentServer(Server):
    """
    The AgentServer is responsible to establish new connections to monitors
    listening for monitors that want to receive information for this agent
    """

    def __init__(self, host="127.0.0.1", port=9000):
        super().__init__(host, port)
        self.resultsQueue = Queue(maxsize=5)
        self.logger = logger.create("agent")
        self.supportedTasks = [
            OSTask(self.logger, self.resultsQueue),
            RAMTask(self.logger, self.resultsQueue),
            CPUTask(self.logger, self.resultsQueue),
            ProcessesTask(self.logger, self.resultsQueue),
            PingTask(self.logger, self.resultsQueue),
            MemoryTask(self.logger, self.resultsQueue),
            PortsTask(self.logger, self.resultsQueue),
        ]
        self.registeredMonitors = []

    def _listen_main(self):
        """
         * Open a socket that the agent listening on.
         * The agent will receive message from 'Monitors', validate its structure.
         *  Once validated - if it was successful - resolve the request.
         *  At the end - update the Monitor for the request's status.
        :return:
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_address = (self.host, self.port)
        self.logger.info('started agent-server on %s port %s' % server_address)
        sock.bind(server_address)

        while True:
            try:  # Get the request
                sock.settimeout(None)  # listen without a timeout
                data, address = sock.recvfrom(4096)
                self.logger.info('received %s bytes from %s' % (len(data), address))
            except TimeoutError:
                continue

            try:  # Using jsonSchema to validate the given data structure
                data = strings.bytes_to_json(data)
                validate(instance=data, schema=constants.agentReceivePacketSchema)
                self.logger.info("Monitor request validated successfully")
                action, monitor_data = data["action"], data["monitor"] if "monitor" in data else None
                if action == "register":
                    status = self.register(monitor_data["host"], monitor_data["port"])
                elif action == "unregister":
                    status = self.unregister(monitor_data["host"], monitor_data["port"])
                elif action == "ping":
                    status = True
                else:
                    raise UnsupportedRequest("Got an unsupported action from Monitor: %s" % action)
                response = {"status": "ok" if status else "failure"}
            except (ValidationError, KeyError, JSONDecodeError, UnsupportedRequest) as e:
                response = {"status": "validation failed", "error": str(e)}

            try:  # Return response matching to the validation result
                sock.settimeout(SOCKET_TIMEOUT)  # timeout the response
                bytes_sent = sock.sendto(strings.json_to_bytes(response), address)
                self.logger.info('sent %s bytes back to %s' % (bytes_sent, address))
            except TimeoutError:
                pass

    def listen(self):
        """
        Runs `listener` as a thread
        :return:
        """
        Thread(group=None,
               target=self._listen_main(),
               name="Listener").start()
        self.logger.info("Started Listener Service")

    def register(self, host, port):
        """
        Get a monitor data and register it to future monitoring-tasks results
        :param host: monitor hostname
        :type host: basestring
        :param port: monitor port
        :type port: int
        :return: whether the registration completed successfully
        """
        self.registeredMonitors.append(Server(host, port))
        return True

    def unregister(self, host, port):
        """
        Get a monitor data and unregister it to future monitoring-tasks results
        :param host: monitor hostname
        :type host: basestring
        :param port: monitor port
        :type port: int
        :return: whether the un-registration completed successfully
        """
        for monitor in self.registeredMonitors:
            if monitor.host == host and monitor.port == port:
                self.registeredMonitors.remove(monitor)
                return True
        return False

    def monitor(self):
        """
         Start the different supported monitors-tasks, each one on a separate thread
        :returns: None
        """
        for monitoringTask in self.supportedTasks:
            if monitoringTask.isPeriodic:
                monitoringTask.periodic_run()
            else:
                monitoringTask.run()

    def _notifier_main(self):
        """
        Send the monitoring-tasks results as reports to the registered Monitors.
        This component also runs in a different thread using the `notifier` function.
        :return:
        """
        while True:
            if not self.resultsQueue.empty():
                self.logger.info("Sending data")
                (task_name, packet_data) = self.resultsQueue.get()
                packet = {
                    "action": "report",
                    "data": packet_data,
                    "agent": {
                        "host": self.host,
                        "port": self.port
                    },
                    "task": task_name
                }

                if len(self.registeredMonitors) == 0:
                    self.logger.info("There are no registered Monitors")
                    continue

                for i in range(len(self.registeredMonitors)):
                    monitor = self.registeredMonitors[i]
                    self.logger.info("Sending report %s out of %s" % (i + 1, len(self.registeredMonitors)))
                    status = sockets.send_packet(self.logger, packet, monitor)
                    self.logger.info("packet sent %s" % "successfully" if status else "with errors")

    def notifier(self):
        """
        Runs `notifier` as a thread
        :return:
        """
        Thread(group=None,
               target=self._notifier_main,
               name="Notifier").start()
        self.logger.info("Started Notify Service")
