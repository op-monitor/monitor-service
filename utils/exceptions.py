class UnsupportedRequest(Exception):
    """
    Received Unsupported request.
    warning severity, won't cause service malfunction
    """
    pass
