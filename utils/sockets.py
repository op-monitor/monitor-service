import socket

from utils import strings

SOCKET_TIMEOUT = 2  # seconds


def _send(logger, data, host, port, resp_flag=False):
    """
    Sending data to given host using UDP protocol.
    Expecting to receive outcome according to the resp flag
    :param data: json
    :type data: dict
    :param host: server hostname
    :type host: basestring
    :param port: server port
    :type port: int
    :type resp_flag: bool
    :return:
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(SOCKET_TIMEOUT)
    server_address = (host, port)
    try:
        # Send data
        sock.sendto(strings.json_to_bytes(data), server_address)

        # Receive response
        if resp_flag:  # validate the response message
            logger.info('waiting to receive')
            sock.settimeout(SOCKET_TIMEOUT)
            resp, server = sock.recvfrom(4096)
            resp = strings.bytes_to_json(resp)
            logger.info('received "%s"' % resp)
            if resp["status"] == "ok":
                return True
            return False

        return True

    except (TimeoutError, socket.error):
        logger.warn("Received timeout, ignoring")

    finally:
        sock.close()


def send_packet(logger, data, server, resp_flag=False):
    """
    Send a data, to a given server
    :param logger: an logger object
    :param resp_flag: wait for response or not
    :param data: json
    :param server: Monitor|Agent
    :return:
    """
    try:
        return _send(logger, data, server.host, server.port, resp_flag)
    except (ConnectionError, ConnectionRefusedError, TimeoutError):
        logger.warn("Failed to send data to server %s" % server.toString())
