import json


def json_to_bytes(data):
    """
    :param data: the data to convert
    :type data: dict
    :return: convert given json to bytes
    """
    return json.dumps(data).encode('utf-8')


def bytes_to_json(data):
    """
    :param data: the data to convert
    :type data: basestring
    :return: convert given bytes to valid json
    """
    return json.loads(data.decode('utf-8'))
