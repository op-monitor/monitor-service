import time
import traceback


def every(delay, task):
    """
    Set a task running after given delay
    :param delay: time b\w tasks, in seconds
    :param task: the function that will be executed
    :return:
    """
    next_time = time.time() + delay
    while True:
        time.sleep(max(0, next_time - time.time()))
        try:
            task()
        except Exception:
            traceback.print_exc()
            # in production code you might want to have this instead of course:
            # logger.exception("Problem while executing repetitive task.")
        # skip tasks if we are behind schedule:
        next_time += (time.time() - next_time) // delay * delay + delay
