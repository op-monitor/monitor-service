import logging
import os

LOGS_LOCATION = 'D:/projects/op-service/logs/'
FILE_NAME_FORMAT = os.path.join(LOGS_LOCATION, "{name}.log")
LOGS_FORMAT = '%(asctime)s : %(levelname)s : %(name)s : %(message)s'


def create(name):
    filename = FILE_NAME_FORMAT.format(name=name)

    # Gets or creates a logger
    logger = logging.getLogger(name)

    # set log level
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(LOGS_FORMAT)

    # define file handler and set formatter
    file_handler = logging.FileHandler(filename)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    # define screen handler and set formatter
    screen_handler = logging.StreamHandler()
    screen_handler.setFormatter(formatter)
    logger.addHandler(screen_handler)

    return logger
