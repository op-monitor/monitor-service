SECRET = "1qaz2wsx"

########### URLs ############

WEB_API_URL_BASE = "http://localhost:8080/api"
SEND_REPORT_URL = WEB_API_URL_BASE + "/report"


########### Payloads ############

def MONITOR_BASE_PAYLOAD(host, port):
    return {
        "monitor":
            {
                "host": host,
                "port": port
            },
        "secret": SECRET
    }


def REGISTER_PAYLOAD(h, p):
    return {**MONITOR_BASE_PAYLOAD(h, p), **{"action": "register"}}


def UNREGISTER_PAYLOAD(h, p):
    return {**MONITOR_BASE_PAYLOAD(h, p), **{"action": "unregister"}}


########### Schemas ############

monitorReceivePacketSchema = {
    "type": "object",
    "properties": {
        "action": {
            "type": "string",
            "enum": ["register", "unregister", "report", "ping"]
        },
        "agent": {
            "type": "object",
            "properties": {
                "host": {
                    "type": "string"
                },
                "port": {
                    "type": "integer"
                }
            },
            "required": [
                "host",
                "port"
            ]
        }
    },
    "required": [
        "action"
    ]
}

agentReceivePacketSchema = {
    "type": "object",
    "properties": {
        "action": {
            "type": "string",
            "enum": ["register", "unregister", "ping"]
        },
        "monitor": {
            "type": "object",
            "properties": {
                "host": {
                    "type": "string"
                },
                "port": {
                    "type": "integer"
                }
            },
            "required": [
                "host",
                "port"
            ]
        },
        "secret": {
            "type": "string",
            "value": SECRET
        }
    },
    "required": [
        "action"
    ]
}
