from modules.agent import AgentServer


if __name__ == "__main__":
    ag = AgentServer("127.0.0.1", 9000)
    ag.monitor()
    ag.notifier()
    ag.listen()
